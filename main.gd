extends Node

#précise quelle est la scene du mob pour les faire apparaître
@export var mob_scene : PackedScene #configuré dans l'interface pour lui dire que c'est une scène mob

var score

func game_over():
	$Music.stop()
	$GameOverSound.play()
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HeadsUpDisplay.show_game_over()

func new_game():
	$Music.play()
	score = 0
	$Player.start($StartPlayerPosition.position)
	$StartTimer.start()
	$HeadsUpDisplay.update_score(score)
	$HeadsUpDisplay.show_message("GET READY")

func _on_start_timer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()


func _on_score_timer_timeout():
	score += 1
	$HeadsUpDisplay.update_score(score)


func _on_mob_timer_timeout():
	var mob = mob_scene.instantiate()
	
	# prend un point aléatoire entre le début et la fin du cadre que l'on a défini
	var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
	
	#permet de faire partir le mob perpendiculairement à la ligne d'apparition dans le bon sens
	var direction = mob_spawn_location.rotation + PI/2
	
	#direction = 1.571  #descend
	#direction = 0 #va à droite
	#direction = 3.142 #va à gauchex
	#direction = 4.712 #monte 
	
	#notre mob est prêt il a une position de départ et un sens de marche
	mob_spawn_location.progress_ratio = randf()
	mob.position = mob_spawn_location.position
	
	#vitesse de déplacement aléatoire comprise entre 150 et 200px par seconde
	var velocity = Vector2(randf_range(150.0, 250.0), 0.0)
	 #0.00 si on veut que la montée/descente regarde au même endroit
	mob.linear_velocity = velocity.rotated(direction)
	
	#ajoute l'instance que l'on vient de faire apparaître à la scene
	add_child(mob)

extends Node2D

@onready var player = $LPCAnimatedSprite2D as LPCAnimatedSprite2D
var last_input = null

#permet d'émettre un signal (ici lorsqu'il entre en collision)
signal hit

# le mot clé export permet d'afficher
@export var speed = 400
var screen_size

# Called when the node enters the scene tree for the first time.
func _ready():
	# permet de récupérer la taille de l'écran et de la stocker dans screen_size
	screen_size = get_viewport_rect().size
	#cache l'objet player par défaut (pour afficher un écran start)
	hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#si on ne touche pas le clavier, on ne déplace pas le perso
	var velocity = Vector2.ZERO 
	
	if Input.is_action_pressed("move_right"):
		player.play(LPCAnimatedSprite2D.LPCAnimation.WALK_RIGHT)
		velocity.x += 1
		last_input = "right"
	if Input.is_action_pressed("move_left"):
		player.play(LPCAnimatedSprite2D.LPCAnimation.WALK_LEFT)
		velocity.x -= 1
		last_input = "left"
	if Input.is_action_pressed("move_down"):
		player.play(LPCAnimatedSprite2D.LPCAnimation.WALK_DOWN)
		velocity.y += 1
		last_input = "down"
	if Input.is_action_pressed("move_up"):
		player.play(LPCAnimatedSprite2D.LPCAnimation.WALK_UP)
		velocity.y -= 1
		last_input = "up"
		
	# si on bouge 
	if velocity.length() > 0:
		# normaliser un vecteur = définir sa taille entre 0 et 1 pour garder un mouvement neutre 
		#(et éviter d'aller plus vite quand on se déplace en diagonale)
		velocity = velocity.normalized() * speed;
	else :
		if last_input == "left":
			player.play(LPCAnimatedSprite2D.LPCAnimation.IDLE_LEFT)
		if last_input == "right":
			player.play(LPCAnimatedSprite2D.LPCAnimation.IDLE_RIGHT)
		if last_input == "down":
			player.play(LPCAnimatedSprite2D.LPCAnimation.IDLE_DOWN)
		if last_input == "up":
			player.play(LPCAnimatedSprite2D.LPCAnimation.IDLE_UP)
			
	# permet de déplacer le personnage au fil du temps (delta) de façon progressive
	position += velocity * delta
	position = position.clamp(Vector2.ZERO, screen_size)

#collision entre joueurs et ennemis 
func _on_body_entered(body):
	hide()
	#handle un signal
	hit.emit()
	#une fois que le joueur est touché on désactive la collision pour éviter que le signal soit joué en boucle
	$CollisionShape2D.set_deferred("disabled", true)
	

#méthode pour initialiser le joueur quand on le souhaitera
func start(pos):
	position = pos
	show()
	#collision activée au début
	$CollisionShape2D.disabled = false

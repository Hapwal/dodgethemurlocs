extends RigidBody2D


func _ready():
	var mob_types = $AnimatedSprite2D.sprite_frames.get_animation_names()
	#prend aléatoirement un type de monstre et l'attribue à play()
	$AnimatedSprite2D.play(mob_types[randi() % mob_types.size()])
	#$AnimatedSprite2D.play("skeleton")
	
	#tourner les mob qui marchent vers la gauche vu que le sprite est à droite par défaut
	#var tutu = get_property_list()
	#var test = linear_velocity.x
	#var osef = "toto"
	if (linear_velocity.x < 0):
		$AnimatedSprite2D.flip_h = true

func _on_visible_on_screen_notifier_2d_screen_exited():
	#destruction de l'objet à fin de la frame
	queue_free()

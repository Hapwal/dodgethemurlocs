extends CanvasLayer

signal start_game

func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()

func show_game_over():
	show_message("GAME OVER")
	#attend 2 secondes comme configuré dans MessageTimer (interface)
	await $MessageTimer.timeout
	
	#réaffiche le message de base
	$Message.text = "DODGE THE ENNEMIES !"
	$Message.show()
	
	#attendre une seconde (timer custom)
	await get_tree().create_timer(1.0).timeout 
	$StartButton.show()

func update_score(score):
	$ScoreLabel.text = str(score)

func _on_start_button_pressed():
	$StartButton.hide()
	#émission de l'évenement start game pour que les autres noeuds le sachent
	start_game.emit()

func _on_message_timer_timeout():
	$Message.hide()
